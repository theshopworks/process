<?php

declare(strict_types=1);

namespace Shopworks\Tests\Unit\Process;

use Shopworks\Process\Process;
use Shopworks\Process\Processor;
use Shopworks\Tests\UnitTestCase;

class ProcessorTest extends UnitTestCase
{
    /** @var Processor $processor */
    private $processor;

    public function setUp(): void
    {
        parent::setUp();

        $this->processor = new Processor();
    }

    /**
     * @test
     */
    public function it_can_run_a_process(): void
    {
        $process = \Mockery::mock(Process::class);
        $process->shouldReceive('setTimeout')->with(3600)->once();
        $process->shouldReceive('run')->once();

        $this->assertInstanceOf(Process::class, $this->processor->process($process));
    }
}
