<?php

namespace Shopworks\Process;

class Processor
{
    public function process(Process $process, bool $realTime = false): Process
    {
        $process->setTimeout(3600);

        if ($realTime) {
            return $this->processRealTime($process);
        }

        $process->run();

        return $process;
    }

    private function processRealTime(Process $process): Process
    {
        $process->setIdleTimeout(600);

        $process->run(function ($type, $buffer): void {
            echo $buffer;
        });

        return $process;
    }
}
