<?php

declare(strict_types=1);

namespace Shopworks\Process;

use Symfony\Component\Process\Process as SymfonyProcess;

class Process extends SymfonyProcess
{
    public const EXIT_CODE_SUCCESS = 0;
    public const EXIT_CODE_FAILED = 1;

    public function __construct(
        array $commandline,
        ?string $cwd = null,
        ?array $env = null,
        $input = null,
        float $timeout = 60
    ) {
        parent::__construct($commandline, $cwd, $env, $input, $timeout);
    }

    public function simple(string $command): self
    {
        return static::fromShellCommandline($command);
    }

    public function getOutput()
    {
        return \trim(parent::getOutput());
    }
}
