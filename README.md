Process
================

A ShopWorks wrapper for the Symfony Process component.

- [Installation](#installation)
- [Code Style](#code-style)
- [Testing](#testing)
- [License](#license)

Installation
------------

Add the `process` package to your `composer.json` file.

``` json
{
    "require": {
        "theshopworks/process": "^0.1"
    }
}
```

Or via the command line in the root of your project.

``` bash
$ composer require "theshopworks/process:^0.1"
```

Code Style
-------

This project follows the following code style guidelines:

- [PSR-2](http://www.php-fig.org/psr/psr-2/) & [PSR-4](http://www.php-fig.org/psr/psr-4/) coding style guidelines.
- Some chosen [PHP-CS-Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer) rules.


``` bash
$ php vendor/bin/php-cs-fixer fix
```


Testing
-------

``` bash
$ php vendor/bin/phpunit
```

License
-------

The MIT License (MIT). Please see [License File](https://gitlab.com/theshopworks/process/blob/master/LICENSE) for more information.
